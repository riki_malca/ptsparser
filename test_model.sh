#!/bin/bash 

display_usage() { 
	echo "Please check the input for this script:"
	echo "1 - the output folder name in which the models will be created."
	echo "2 - the flag model usage: use \"Dual_learning\" or \"None\"."
	echo "3 - Additional parameters. write all the parameters for the parser within quotation marks:"
	echo "		*)--is_segmentation_model_used - for using the SEG model."
	echo "		*)--is_extanded_segmentation_model_used - for using the FULL_SEG model (use with --is_segmentation_model_used)."
	echo "		*)--is_name_entity_use - for using the named entity tagger."
	echo "		*)--name_entity_middle_layer - for using the named entity tagger hidden layer vector (Hi)."
	echo "		*)--name_entity_scores_layer - for using the named entity tagger final layer vector (Fi)."
	echo "		*)--name_entity_join - for training the named entity model jointly with the parser."
	echo "4 - path to test file."
	echo "5 - path to the flag model."
	echo "6 - path to the segmentation model or \"None\" if you wish to train the model before the parser training."
	echo "7 - path to the params file of the segmentation model or \"None\" if you wish to train the model before the parser training."
	echo "8 - path to the model tested."
	echo "9 - path to the params of the model tested."
	echo "10 - path to the ner model tested."
	} 


# if less than seven arguments supplied, display usage 
	if [  $# -le 9 ] 
	then 
		display_usage
		exit 1
	fi 
 
# check whether user had supplied -h or --help . If yes display usage 
	if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
		display_usage
		exit 0
	fi 

mkdir $1 

python src/parser.py --predict --model $8 --params $9 --segmodel $6 --segmodel_params $7 --flag_model $5 --ne_model $10 --dynet-seed 123456789 --outdir $1 --test $4 --epochs 30 --lstmdims 125 --lstmlayers 2 --bibi-lstm --k 3 --usehead --userl --is_flag_model_used $2 $3 

#
#
#./test_model.sh  "Dual_learning" "--is_segmentation_model_used" test_set1_tagged.conll src/output/test/flag_barchybrid.model30 seg_barchybrid.model30  params_seg.pickle src/output/test/barchybrid.model30 src/output/test/params.pickle