# Segmentation aware Parsers

#### Required software
-----------------------
 * Python 2.7 interpreter
 * [DyNet library](https://github.com/clab/dynet/tree/master/python)

#### Files format
-------------------
The software requires having a train, dev and test files formatted according to the [CoNLL data format](http://ilk.uvt.nl/conll/#dataformat).
If a named entity tagger is used, the files must have an additional column with named entity tags:
The available tags are:
(1) Not a named entity.
(2) Location name.
(3) Person name.
(4) Organization name.
(5) Product name.
(6) Other kind of name.
(See example.conll file).

#### Data
-----------
The annotated training, test and dev sets will be uploaded soon.

#### Training
---------------
The software requires having a `training.conll` and `development.conll`.

##### Training the parsing model

To train a parsing model with for either parsing architecture type the following at the command prompt:

    python src/parser.py --dynet-seed 123456789 [--dynet-mem XXXX] --outdir [results directory] --train training.conll --dev development.conll --epochs 30 --lstmdims 125 --lstmlayers 2 --bibi-lstm  --k 3 --usehead --userl

Additional arguments : 
-) Using the SEG model:
	add a "--segmodel [seg model]" to point to the SEG model will be used by the software and a flag argument "--is_segmentation_model_used".
	For FULL_SEG useage, use an additional argument: "--is_extanded_segmentation_model_used".
-) Using the FLAG model:
	For joint training with the FLAG model use an additional argument "--is_flag_model_used Dual_learning".
-) Using the NE tagger:
	For named entity tagger usage add an additional flag argument: "--is_name_entity_use".
	Usage Options:
	*) Use "--name_entity_middle_layer" for using the named entity tagger hidden layer vector.
	*) Default: the software is using the named entity tagger final decition embeddings.
	Training options:
	*) Joint Training:
		Use an argument: "--name_entity_join".
	*) Independent training:
		Add "--ne_model [ne model]". 
-) Using the Arc-eager parser:
	add the argument "--base_parser".

##### Training the SEG model
	Use the flag segument "--train_segmentation".

##### Training the NE parser
	Use the flag segument "--train_name_entity".

##### Parse data with your parsing model
-----------------------------------------
Add to the training command the flag argument "--predict" and a "--test testing.conll" argument.


#### Scripts
------------
For your convenience, the scripts train_model.sh and test_model.sh are attached for training and testing.
The usage is specified using the --help parameter.

#### Forks
------------
[BIST-PyTorch](https://github.com/wddabc/bist-parser): A PyTorch implementation of the BIST Parsers (for graph based parser only). 

[BIST-COVINGTON](https://github.com/aghie/LyS-FASTPARSE): A neural implementation of the Covington's algorithm for non-projective dependency parsing. It extends the original BIST transition-based a greedy parser by including a dynamic oracle for non-projective parsing to mitigate error propagation.

[Uppsala Parser](https://github.com/UppsalaNLP/uuparser):  A transition-based parser for Universal Dependencies with BiLSTM word and character representations. 

[BIST Parsers] (https://github.com/elikip/bist-parser): Graph & Transition based dependency parsers using BiLSTM feature extractors.


