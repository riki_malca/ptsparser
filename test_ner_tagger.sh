#!/bin/bash 

display_usage() { 
	echo "Please check the input for this script:"
	echo "1 - the output folder name in which the models will be created."
	echo "2 - path to test file."
	echo "3 - path to the ner model."
	echo "4 - path to the params file."

	} 


# if less than seven arguments supplied, display usage 
	if [  $# -le 3 ] 
	then 
		display_usage
		exit 1
	fi 
 
# check whether user had supplied -h or --help . If yes display usage 
	if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
		display_usage
		exit 0
	fi 

mkdir $1 

python src/parser.py --train_name_entity --predict --ne_model $3 --params $4 --dynet-seed 123456789 --outdir $1 --test $2 --epochs 30 --lstmdims 125 --lstmlayers 2 --bibi-lstm --k 3 --usehead --userl 