#!/bin/bash 

display_usage() { 
	echo "Please check the input for this script:"
	echo "1 - the output folder name in which the models will be created."
	echo "2 - the flag model usage: use \"Dual_learning\" or \"None\"."
	echo "3 - Additional parameters. write all the parameters for the parser within quotation marks:"
	echo "		*)--is_segmentation_model_used - for using the SEG model."
	echo "		*)--is_extanded_segmentation_model_used - for using the FULL_SEG model (use with --is_segmentation_model_used)."
	echo "		*)--is_name_entity_use - for using the named entity tagger."
	echo "		*)--name_entity_middle_layer - for using the named entity tagger hidden layer vector (Hi)."
	echo "		*)--name_entity_scores_layer - for using the named entity tagger final layer vector (Fi)."
	echo "		*)--name_entity_join - for training the named entity model jointly with the parser."
	echo "4 - path to train file."
	echo "5 - path to dev file."
	echo "6 - path to the segmentation model or \"None\" if you wish to train the model before the parser training."
	echo "7 - path to the params file of the segmentation model or \"None\" if you wish to train the model before the parser training."
	echo "8 - path to the named entity tagging model or \"None\" if you use joint training."
	} 


# if less than seven arguments supplied, display usage 
	if [  $# -le 7 ] 
	then 
		display_usage
		exit 1
	fi 
 
# check whether user had supplied -h or --help . If yes display usage 
	if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
		display_usage
		exit 0
	fi 

mkdir $1 

#cp $6 $1/seg_model.model30 # output/name_entity_seg/seg_model.model30 src/output/seg_barchybrid.model30
#cp $7 $1/params_seg.pickle # output/name_entity_seg/params_seg.pickle src/output/params_seg.pickle

python src/parser.py --segmodel $6 --segmodel_params $7 --ne_model $8 --dynet-seed 123456789 --outdir $1 --train $4 --dev $5 --epochs 30 --lstmdims 125 --lstmlayers 2 --bibi-lstm --k 3 --usehead --userl --is_flag_model_used $2 $3