__author__ = 'riki.malca'

import os
import pickle

import utils
from segmentation_classifier import SegmentationClassifier
from base_bilstm_model import BaseBiLSTMModel


class SegmentationModel(BaseBiLSTMModel):

    def __init__(self, options):
        super(SegmentationModel, self).__init__(options)
        self.MODEL_NAME = "SEG model"
        self._do_train = False
        self._calculate_file_paths()

    def _calculate_file_paths(self):
        self._params_path = self.options.segmodel_params
        self._model_path = self.options.segmodel
        if self.params_path == "None":
            self._params_path = os.path.join(self.options.output, self.options.params + "seg")
            self._do_train = True
        if self._model_path == "None":
            self._do_train = True
            self._model_path = os.path.join(self.options.output, "seg_model.model30")

    @property
    def output_model_name(self):
        return "seg_model.model"

    @property
    def input_model_name(self):
        return self.options.segmodel

    @property
    def do_train(self):
        return self._do_train

    @property
    def model_path(self):
        return self._model_path

    @property
    def params_path(self):
        return self._params_path

    @property
    def parser(self):
        words, w2i, pos, rels = utils.vocab(self.options.conll_train)

        with open(os.path.join(self.options.output, self.options.params + "seg"), 'w') as paramsfp:
            pickle.dump((words, w2i, pos, rels, self.options), paramsfp)
        return SegmentationClassifier(words, pos, rels, w2i, self.options)

    def read_model(self, words, pos, rels, w2i, stored_opt):
        return SegmentationClassifier(words, pos, rels, w2i, stored_opt)