__author__ = 'riki.malca'

import os
import pickle

import utils
from named_entity_tagger import NameEntityTagger
from base_bilstm_model import BaseBiLSTMModel


class NamedEntityModel(BaseBiLSTMModel):
    def __init__(self, options):
        super(NamedEntityModel, self).__init__(options)
        self.MODEL_NAME = "NER model"

    @property
    def output_model_name(self):
        return "name_entity_model.model"

    @property
    def input_model_name(self):
        return self.options.ne_model

    @property
    def input_model_name(self):
        return self.options.ne_model

    @property
    def parser(self):
        words, w2i, pos, rels = utils.vocab(self.options.conll_train)

        with open(os.path.join(self.options.output, self.options.params + "ner"), 'w') as paramsfp:
            pickle.dump((words, w2i, pos, rels, self.options), paramsfp)
        return NameEntityTagger(words, pos, rels, w2i, self.options)

    def read_model(self, words, pos, rels, w2i, stored_opt):
        return NameEntityTagger(words, pos, rels, w2i, stored_opt)