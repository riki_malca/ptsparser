__author__ = 'riki.malca'

import os
import time
import utils
import pickle, utils, os, time, sys
from bilstm import BiLSTM


class BaseBiLSTMModel(object):

    def __init__(self, options):
        self.options = options
        self.MODEL_NAME = "ABSTRACT_PARSER"

    @property
    def output_model_name(self):
        return ""

    @property
    def input_model_name(self):
        return ""

    @property
    def model_path(self):
        return "None"
    @property
    def params_path(self):
        return "None"

    @property
    def parser(self):
        return None

    def _read_params(self):
        with open(self.options.params, 'r') as paramsfp:
                words, w2i, pos, rels, stored_opt = pickle.load(paramsfp)
        stored_opt.external_embedding = self.options.external_embedding
        return words, w2i, pos, rels, stored_opt

    def read_model(self, words, pos, rels, w2i, stored_opt):
        """
        :return: a trained BiLSTM model
        :rtype: BiLSTM
        """
        return None

    def train(self):
        print 'Start training the {model_name}.'.format(model_name=self.MODEL_NAME)
        for epoch in xrange(self.options.epochs):
            print 'Starting epoch', epoch
            self.parser.Train(self.options.conll_train)
            conllu = (os.path.splitext(self.options.conll_dev.lower())[1] == '.conllu')
            devpath = os.path.join(self.options.output,
                                   'dev_epoch_' + str(epoch + 1) + ('.conll' if not conllu else '.conllu'))
            utils.write_conll(devpath, self.parser.Predict(self.options.conll_dev))
            self.parser.Predict(self.options.conll_dev)
            self.parser.Save(os.path.join(self.options.output, self.output_model_name + str(epoch + 1)))
        print 'Finished training the {model_name}.'.format(model_name=self.MODEL_NAME)


    def test(self):
        with open(self.options.params, 'r') as paramsfp:
                words, w2i, pos, rels, stored_opt = pickle.load(paramsfp)
        stored_opt.external_embedding = self.options.external_embedding
        parser = self.read_model(words, pos, rels, w2i, stored_opt)
        parser.Load(self.input_model_name)
        conllu = (os.path.splitext(self.options.conll_test.lower())[1] == '.conllu')
        tespath = os.path.join(self.options.output, 'test_pred_{model}.conll'.format(model=self.MODEL_NAME) if not conllu else 'test_pred_{model}.conllu'.format(model=self.MODEL_NAME) )
        ts = time.time()
        pred = parser.Predict(self.options.conll_test)
        te = time.time()
        utils.write_conll(tespath, pred)
        print 'Finished predicting test for model {model}.'.format(model=self.MODEL_NAME) , te - ts