__author__ = 'riki.malca'

import os
import pickle

import utils
from segmentation_model import SegmentationModel
from pts import PTSLSTM
from arc_eager_base import ArcEagerLSTMBase
from base_bilstm_model import BaseBiLSTMModel


class PTSModel(BaseBiLSTMModel):
    def __init__(self, options, seg_model):
        assert isinstance(seg_model, SegmentationModel)

        super(PTSModel, self).__init__(options)
        if self.options.base_parser:
            self.MODEL_NAME = "ArcEager model"
        else:
            self.MODEL_NAME = "PTS model"
        self.seg_model = seg_model

    @property
    def output_model_name(self):
        return self.options.model


    @property
    def input_model_name(self):
        return self.options.model

    @property
    def parser(self):
        words, w2i, pos, rels = utils.vocab(self.options.conll_train)
        with open(os.path.join(self.options.output, self.options.params), 'w') as paramsfp:
            pickle.dump((words, w2i, pos, rels, self.options), paramsfp)
        if self.seg_model.do_train:
            self.seg_model.train()
        if self.options.base_parser:
            return ArcEagerLSTMBase(words, pos, rels, w2i, self.options)
        else:
            return PTSLSTM(words, pos, rels, w2i, self.options, self.seg_model.model_path, self.seg_model.params_path)

    def read_model(self, words, pos, rels, w2i, stored_opt):
        if self.options.base_parser:
            return ArcEagerLSTMBase(words, pos, rels, w2i, self.options)
        return PTSLSTM(words, pos, rels, w2i, self.options, self.seg_model.model_path, self.seg_model.params_path)