from dynet import *
from utils import ParseForest, read_conll, write_conll
from operator import itemgetter
from bilstm import BiLSTM
from itertools import chain
import utils, time, random
import numpy as np


class SegmentationClassifier(BiLSTM):
    def init_segmentation_model(self, options):
        self.NOT_SEGMENTATION_POINT = 0
        self.SEGMENTATION_POINT = 1

        self.segmentation_model = Model()
        self.segmentation_trainer = AdamTrainer(self.segmentation_model)
        self.segmentation_activations = {'tanh': tanh, 'sigmoid': logistic, 'relu': rectify, 'tanh3': (lambda x: tanh(cwise_multiply(cwise_multiply(x, x), x)))}
        self.segmentation_activation = self.activations[options.activation]
        if self.activation_relu:
            self.segmentation_activation = self.activations['relu']
        # 250 * 100 * 25* 0
        # hidden units = 100 k  is the window size
        # hidden = 100
        # nnvecs = 3
        self.segmentation_hidLayer = self.segmentation_model.add_parameters((self.hidden_units, self.ldims * self.nnvecs))
        self.segmentation_outLayer = self.segmentation_model.add_parameters((2, self.hidden_units))
        self.segmentation_hidBias = self.segmentation_model.add_parameters((self.hidden_units))
        self.segmentation_outBias = self.segmentation_model.add_parameters((2))



    def __init__(self, words, pos, rels, w2i, options):
        self.activation_relu = True
        self.is_dynamic = True
        self.is_ra = True
        if self.is_ra:
            self.number_of_transitions = 5
        else:
            self.number_of_transitions = 4
        if self.is_ra:
            self.number_of_non_relational_transitions = 3 # reduce & shift are non relational transitions.
        else:
            self.number_of_non_relational_transitions = 2
        self.number_of_relational_transitions = self.number_of_transitions - self.number_of_non_relational_transitions
        self.activations = {'tanh': tanh, 'sigmoid': logistic, 'relu': rectify,
                            'tanh3': (lambda x: tanh(cwise_multiply(cwise_multiply(x, x), x)))}
        self.activation = self.activations[options.activation]

        # transitions:
        self.SHIFT=0
        self.LEFT_ARC=1
        self.RIGHT_ARC=2
        self.REDUCE = 3
        if self.is_ra:
            self.REDUCE_ALL = 4 # New
        self.oracle = options.oracle
        self.ldims = options.lstm_dims * 2
        self.wdims = options.wembedding_dims
        self.pdims = options.pembedding_dims
        self.rdims = options.rembedding_dims
        self.layers = options.lstm_layers
        self.wordsCount = words
        self.vocab = {word: ind + 3 for word, ind in w2i.iteritems()}
        self.pos = {word: ind + 3 for ind, word in enumerate(pos)}
        self.pos['.'] = self.pos['NN']
        self.rels = {word: ind for ind, word in enumerate(rels)}
        self.irels = rels
        # --------------
        self.headFlag = options.headFlag
        self.rlMostFlag = options.rlMostFlag
        self.rlFlag = options.rlFlag
        self.k = options.window
        self.hidden_units = options.hidden_units
        self.hidden2_units = options.hidden2_units
        self.vocab['*PAD*'] = 1
        self.pos['*PAD*'] = 1
        self.external_embedding = None
        self.name_entity_dims = 1

        if options.external_embedding is not None:
            external_embedding_fp = open(options.external_embedding, 'r')
            external_embedding_fp.readline()
            self.external_embedding = {line.split(' ')[0]: [float(f) for f in line.strip().split(' ')[1:]] for line in
                                       external_embedding_fp}
            external_embedding_fp.close()

            self.edim = len(self.external_embedding.values()[0])
            self.noextrn = [0.0 for _ in xrange(self.edim)]
            self.extrnd = {word: i + 3 for i, word in enumerate(self.external_embedding)}
            self.elookup = self.segmentation_model.add_lookup_parameters(
                (len(self.external_embedding) + 3, self.edim))  # not coming here
            for word, i in self.extrnd.iteritems():
                self.elookup.init_row(i, self.external_embedding[word])
            self.extrnd['*PAD*'] = 1
            self.extrnd['*INITIAL*'] = 2

            print 'Load external embedding. Vector dimensions', self.edim

        dims = self.name_entity_dims + self.wdims + self.pdims + (self.edim if self.external_embedding is not None else 0)
        self.blstmFlag = options.blstmFlag
        self.bibiFlag = options.bibiFlag
        self.nnvecs = (1 if self.headFlag else 0) + (2 if self.rlFlag or self.rlMostFlag else 0)
        self.init_segmentation_model(options)
        if self.bibiFlag:
            self.surfaceBuilders = [VanillaLSTMBuilder(1, dims, self.ldims * 0.5, self.segmentation_model),
                                    VanillaLSTMBuilder(1, dims, self.ldims * 0.5, self.segmentation_model)]
            self.bsurfaceBuilders = [VanillaLSTMBuilder(1, self.ldims, self.ldims * 0.5, self.segmentation_model),
                                     VanillaLSTMBuilder(1, self.ldims, self.ldims * 0.5, self.segmentation_model)]
        elif self.blstmFlag:
            if self.layers > 0:
                self.surfaceBuilders = [VanillaLSTMBuilder(self.layers, dims, self.ldims * 0.5, self.segmentation_model), LSTMBuilder(self.layers, dims, self.ldims * 0.5, self.segmentation_model)]
            else:
                self.surfaceBuilders = [SimpleRNNBuilder(1, dims, self.ldims * 0.5, self.segmentation_model), LSTMBuilder(1, dims, self.ldims * 0.5, self.segmentation_model)]

        self.vocab['*INITIAL*'] = 2
        self.pos['*INITIAL*'] = 2

        # words length is 2632 and contains all types of words (from the training) and dimention in the options : 100 - word dimentions
        self.wlookup = self.segmentation_model.add_lookup_parameters((len(words) + 3, self.wdims))
        self.plookup = self.segmentation_model.add_lookup_parameters((len(pos) + 3, self.pdims)) # every part of speach is represented by 25 dimention
        self.rlookup = self.segmentation_model.add_lookup_parameters((len(rels), self.rdims))  # two type of relations - with/without root , each represented by 25
        self.nameEntitylookup = self.segmentation_model.add_lookup_parameters((6 + 1, self.name_entity_dims))

        self.word2lstm = self.segmentation_model.add_parameters((self.ldims, self.name_entity_dims + self.wdims + self.pdims + (self.edim if self.external_embedding is not None else 0)))
        self.word2lstmbias = self.segmentation_model.add_parameters((self.ldims))
        self.lstm2lstm = self.segmentation_model.add_parameters((self.ldims, self.ldims * self.nnvecs + self.rdims))
        self.lstm2lstmbias = self.segmentation_model.add_parameters((self.ldims))


    def get_reduce_index(self):
        # left arc and right arc are before reduce
        return len(self.irels) * 2 + 1

    def get_reduce_all_index(self):
        # left arc and right arc are before reduce
        return len(self.irels) * 2 + 2

    def is_reduce_all(self, stack):
        for item in stack.roots[1:]:
            if item.parent is None:
                return False
        return True

    def __evaluate(self, word):

        input = concatenate(list(chain(*([word.lstms]))))

        # new ra evaluation
        middle_exp = self.activation(self.segmentation_hidLayer.expr() * input + self.segmentation_hidBias.expr())
        segmentation_output = (self.segmentation_outLayer.expr() * middle_exp + self.segmentation_outBias.expr())
        segmentation_output_value = segmentation_output.value()
        seg_ret = [
            [self.NOT_SEGMENTATION_POINT, segmentation_output_value[self.NOT_SEGMENTATION_POINT], segmentation_output[self.NOT_SEGMENTATION_POINT]],
            [self.SEGMENTATION_POINT, segmentation_output_value[self.SEGMENTATION_POINT], segmentation_output[self.SEGMENTATION_POINT]]
        ]

        return  seg_ret



    def Save(self, file_name):
        self.segmentation_model.save(file_name)

    def Load(self, file_name):
        self.segmentation_model.populate(file_name)


    def Init(self):
        evec = self.elookup[1] if self.external_embedding is not None else None
        paddingWordVec = self.wlookup[1]
        paddingPosVec = self.plookup[1] if self.pdims > 0 else None
        paddingNameEntityVec = self.nameEntitylookup[0]

        paddingVec = tanh(self.word2lstm.expr() * concatenate(
            filter(None, [paddingWordVec, paddingPosVec, evec, paddingNameEntityVec])) + self.word2lstmbias.expr())
        self.empty = paddingVec if self.nnvecs == 1 else concatenate([paddingVec for _ in xrange(self.nnvecs)])

    def getWordEmbeddings(self, sentence, train):
        for root in sentence:
            c = float(self.wordsCount.get(root.norm, 0))
            dropFlag = not train or (random.random() < (c / (0.25 + c)))
            root.wordvec = self.wlookup[int(self.vocab.get(root.norm, 0)) if dropFlag else 0]
            if root.pos == "TMOD":
                root.pos = "-"
            if root.pos == "PARATAXIS":
                root.pos = "-"
            if root.pos == "REF":
                root.pos = "-"
            try:
                root.posvec = self.plookup[int(self.pos[root.pos])] if self.pdims > 0 else None
            except Exception as e:
                print("missing pos {pos}".format(pos=root.pos))
                print(self.pos.keys())
                pass
            if self.external_embedding is not None:
                # if not dropFlag and random.random() < 0.5:
                #    root.evec = self.elookup[0]
                if root.form in self.external_embedding:
                    root.evec = self.elookup[self.extrnd[root.form]]
                elif root.norm in self.external_embedding:
                    root.evec = self.elookup[self.extrnd[root.norm]]
                else:
                    root.evec = self.elookup[0]
            else:
                root.evec = None
            root.name_entity_vec = self.nameEntitylookup[root.name_entity]
            root.ivec = concatenate(filter(None, [root.wordvec, root.posvec, root.evec, root.name_entity_vec]))

        if self.blstmFlag:
            forward = self.surfaceBuilders[0].initial_state()
            backward = self.surfaceBuilders[1].initial_state()

            for froot, rroot in zip(sentence, reversed(sentence)):
                forward = forward.add_input(froot.ivec)
                backward = backward.add_input(rroot.ivec)
                froot.fvec = forward.output()
                rroot.bvec = backward.output()
            for root in sentence:
                root.vec = concatenate([root.fvec, root.bvec])

            if self.bibiFlag:
                bforward = self.bsurfaceBuilders[0].initial_state()
                bbackward = self.bsurfaceBuilders[1].initial_state()

                for froot, rroot in zip(sentence, reversed(sentence)):
                    bforward = bforward.add_input(froot.vec)
                    bbackward = bbackward.add_input(rroot.vec)
                    froot.bfvec = bforward.output()
                    rroot.bbvec = bbackward.output()
                for root in sentence:
                    root.vec = concatenate([root.bfvec, root.bbvec])

        else:
            for root in sentence:
                root.ivec = (self.word2lstm.expr() * root.ivec) + self.word2lstmbias.expr()
                root.vec = tanh(root.ivec)

    def init_conll_sentence(self, sentence):
        conll_sentence = [entry for entry in sentence if isinstance(entry, utils.ConllEntry)]
        self.getWordEmbeddings(conll_sentence, False)
        for root in conll_sentence:
            root.lstms = [root.vec for _ in xrange(self.nnvecs)]
        return conll_sentence


    def Predict(self, conll_path):
        total = 0
        success= 0
        errors = 0
        tp = 0
        number_of_correct_possitive_in_gold = 0
        number_of_correct_possitive_in_parser = 0
        tn = 0
        number_of_correct_negative_in_gold = 0
        number_of_correct_negative_in_parser = 0
        full_seg_eval_tp = 0
        full_seg_eval_parser_segments_positive = 0
        full_seg_eval_gold_segments_positive = 0
        with open(conll_path, 'r') as conllFP:
            for iSentence, sentence in enumerate(read_conll(conllFP, False)):
                self.Init()
                heads = self.get_sentence_heads(sentence[1:])
                segmentations_bounderies = self.get_segmentations_bounderies(heads)
                conll_sentence = self.init_conll_sentence(sentence)
                new_segmentation_bounderies = []
                new_segment = []
                for iword, word in enumerate(conll_sentence):
                    total += 1
                    if iword > 0:
                        new_segment.append(iword )
                    seg_ret = self.__evaluate(word)
                    is_segmentation, loss_scalar = self.is_segmentation_point(segmentations_bounderies, iword, heads)
                    if is_segmentation:
                        number_of_correct_possitive_in_gold += 1
                    else:
                        number_of_correct_negative_in_gold+= 1
                    if seg_ret[self.SEGMENTATION_POINT][1] > seg_ret[self.NOT_SEGMENTATION_POINT][1]:
                        number_of_correct_possitive_in_parser += 1
                        if len(new_segment)>0:
                            new_segmentation_bounderies.append(new_segment)
                        new_segment = []
                    else:
                        number_of_correct_negative_in_parser += 1
                    if is_segmentation and seg_ret[self.SEGMENTATION_POINT][1] > seg_ret[self.NOT_SEGMENTATION_POINT][1]:
                        success += 1
                        tp+=1
                    elif not is_segmentation and seg_ret[self.SEGMENTATION_POINT][1] < seg_ret[self.NOT_SEGMENTATION_POINT][1]:
                        tn += 1
                    elif not is_segmentation and seg_ret[self.SEGMENTATION_POINT][1] < seg_ret[self.NOT_SEGMENTATION_POINT][1]:
                        success += 1
                    else:
                        errors += 1
                if len(new_segment) > 0:
                    new_segmentation_bounderies.append(new_segment)



                full_seg_eval_parser_segments_positive += len(new_segmentation_bounderies)
                full_seg_eval_gold_segments_positive += len(segmentations_bounderies)
                for segment in new_segmentation_bounderies:
                    if segment in segmentations_bounderies:
                        full_seg_eval_tp += 1
                renew_cg()
                yield sentence
        print "Total {total} Success {success} Errors {errors}".format(total=total, success=success, errors=errors)
        print "tp {tp} number_of_correct_positive_in_gold {number_of_correct_in_gold} number_of_correct_possitive_in_parser {number_of_correct_in_parser}".format(tp=tp, number_of_correct_in_gold=number_of_correct_possitive_in_gold, number_of_correct_in_parser=number_of_correct_possitive_in_parser)
        print "tn {tn} number_of_correct_negative_in_gold {number_of_correct_in_gold} number_of_correct_negative__in_parser {number_of_correct_in_parser}".format(tn=tn, number_of_correct_in_gold=number_of_correct_negative_in_gold, number_of_correct_in_parser=number_of_correct_negative_in_parser)
        if number_of_correct_possitive_in_parser == 0:
            precision = 0
        else:
            precision = tp*1.0/number_of_correct_possitive_in_parser
        if number_of_correct_possitive_in_gold == 0:
            recall = 0
        else:
            recall = tp*1.0/number_of_correct_possitive_in_gold
        if precision + recall == 0:
            score_positive = 0
        else:
            score_positive =  2.0 * precision * recall / (precision + recall)
        print "f1 score positive is {score} percition : {per} recall : {rec}".format(score=str(score_positive), per= str(precision) ,rec=str(recall))
        if number_of_correct_negative_in_parser == 0:
            precision = 0
        else:
            precision = tn*1.0/number_of_correct_negative_in_parser
        if number_of_correct_negative_in_gold == 0:
            recall = 0
        else:
            recall = tn*1.0/number_of_correct_negative_in_gold
        if precision + recall == 0:
            score_negative = 0
        else:
            score_negative =  2.0 * precision * recall / (precision + recall)
        print "f1 score negative is {score} percition : {per} recall : {rec}".format(score=str(score_negative), per= str(precision) ,rec=str(recall))
        print "average {avg}".format(avg = str(0.5*(score_negative+score_positive)))
        if full_seg_eval_parser_segments_positive == 0:
            precision = 0
        else:
            precision = full_seg_eval_tp*1.0/full_seg_eval_parser_segments_positive
        if full_seg_eval_gold_segments_positive == 0:
            recall = 0
        else:
            recall = full_seg_eval_tp*1.0/full_seg_eval_gold_segments_positive
        if precision + recall == 0:
            score = 0
        else:
            score =  2.0 * precision * recall / (precision + recall)
        print "Full segmentation f1 score is {score} percition : {per} recall : {rec}".format(score=str(score), per= str(precision) ,rec=str(recall))


    def is_segmentation_point(self, segmentation_boundries, index, heads):
        index = index +1
        if index == len(heads) :
            return False, 0
        for i in range(len(segmentation_boundries)):
            seg = segmentation_boundries[i]
            if index in seg :
                if index == max(seg):
                    if i == len(segmentation_boundries) - 1:
                        return True, 0
                    return True, len(segmentation_boundries[i+1])
                else:
                    if i == len(segmentation_boundries) - 1:
                        return False, len([j for j in seg if j > index])
                    else:
                        return False, len([j for j in seg if j > index])+len(segmentation_boundries[i+1])
        return False, 0

    def get_sentence_heads(self, sentence):
        heads = []
        for line in sentence:
            heads.append(line.parent_id)
        return heads

    def get_segmentations_bounderies(self, heads):
        all_segments = []
        segment = [1]
        for i in range(1, len(heads)):
            # Checking if no out arcs
            is_found = False
            for j in range(i):
                if heads[j] > i and heads[j] != 0:
                    is_found = True
                    break
            # checking if no in arcs
            for j in range(i, len(heads)):
                if heads[j] != 0 and heads[j] <= i:
                    is_found = True
                    break
            if is_found:
                segment.append(i + 1)
            else:
                all_segments.append(segment)
                segment = [i + 1]
        all_segments.append(segment)
        return all_segments

    def Train(self, conll_path):
        ra_mloss = 0.0
        mloss = 0.0
        eloss = 0.0
        eerrors = 0
        lerrors = 0
        etotal = 0
        ninf = -float('inf')

        hoffset = 1 if self.headFlag else 0

        start = time.time()

        with open(conll_path, 'r') as conllFP:
            shuffledData = list(read_conll(conllFP, True))
            random.shuffle(shuffledData)
            seg_errs= []

            self.Init()
            counter =0
            for iSentence, sentence in enumerate(shuffledData):
                if iSentence % 100 == 0 and iSentence != 0:
                    print 'Processing sentence number:', iSentence, 'Loss:', ra_mloss / etotal, 'Errors:', (float(eerrors)) / etotal, 'Labeled Errors:', (float(lerrors) / etotal) , 'Time', time.time()-start
                    start = time.time()
                    eerrors = 0
                    eloss = 0.0
                    etotal = 0
                    lerrors = 0
                heads = self.get_sentence_heads(sentence[1:])
                segmentations_bounderies = self.get_segmentations_bounderies(heads)

                conll_sentence = self.init_conll_sentence(sentence)
                new_segmentation_bounderies = []
                current_segment = []
                losses = {}
                for iword, word in enumerate(conll_sentence):
                    current_segment.append(iword)
                    seg_ret = self.__evaluate(word)
                    losses[iword] = scalarInput(0)
                    is_seg , loss_scalar = self.is_segmentation_point(segmentations_bounderies, iword, heads)

                    if seg_ret[self.SEGMENTATION_POINT][1] > seg_ret[self.NOT_SEGMENTATION_POINT][1]:
                        new_segmentation_bounderies.append(current_segment)
                        current_segment = []
                    else:
                        counter += 1
                    if is_seg:
                        if seg_ret[self.SEGMENTATION_POINT][1] < seg_ret[self.NOT_SEGMENTATION_POINT][1]:
                            ra_loss = seg_ret[self.NOT_SEGMENTATION_POINT][2] - seg_ret[self.SEGMENTATION_POINT][2]
                            seg_errs.append(ra_loss)
                            losses[iword]=ra_loss
                            ra_mloss += 1.0 + seg_ret[self.NOT_SEGMENTATION_POINT][1]-seg_ret[self.SEGMENTATION_POINT][1]
                            eerrors += 1
                    else:
                        if seg_ret[self.SEGMENTATION_POINT][1] > seg_ret[self.NOT_SEGMENTATION_POINT][1]:
                            ra_loss = seg_ret[self.SEGMENTATION_POINT][2] - seg_ret[self.NOT_SEGMENTATION_POINT][2]
                            seg_errs.append(ra_loss)
                            losses[iword]=ra_loss
                            ra_mloss += 1.0 + seg_ret[self.SEGMENTATION_POINT][1] - seg_ret[self.NOT_SEGMENTATION_POINT][1]
                            eerrors += 1

                    etotal += 1
                if len(seg_errs) > 0 :
                    ra_eerrs = esum(seg_errs)
                    ra_eerrs.backward()
                    self.segmentation_trainer.update()
                    seg_errs = []
                    renew_cg()

            print "counter of the times not segment had higher score : {c}".format(c=counter)
            self.segmentation_trainer.update()
            print "seg Loss ", ra_mloss / iSentence
            print "Loss: ", mloss/iSentence
