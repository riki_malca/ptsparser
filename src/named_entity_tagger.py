from dynet import *
from utils import ParseForest, read_conll, write_conll
from operator import itemgetter
from itertools import chain
import utils, time, random
import numpy as np
from bilstm import BiLSTM


class NameEntityTagger(BiLSTM):

    def __init_name_entity_model(self, options):
        self.name_entity_model = Model()
        self.trainer = AdamTrainer(self.name_entity_model)
        self.activations = {'tanh': tanh, 'sigmoid': logistic, 'relu': rectify, 'tanh3': (lambda x: tanh(cwise_multiply(cwise_multiply(x, x), x)))}
        self.activation = self.activations[options.activation]
        self.hidLayer = self.name_entity_model.add_parameters((self.hidden_units, self.ldims * self.nnvecs))
        self.outLayer = self.name_entity_model.add_parameters((6, self.hidden_units))
        self.hidBias = self.name_entity_model.add_parameters((self.hidden_units))
        self.outBias = self.name_entity_model.add_parameters((6))



    def __init__(self, words, pos, rels, w2i, options):
        self.oracle = options.oracle
        self.ldims = options.lstm_dims * 2
        self.wdims = options.wembedding_dims
        self.pdims = options.pembedding_dims
        self.rdims = options.rembedding_dims
        self.layers = options.lstm_layers
        self.wordsCount = words
        self.vocab = {word: ind + 3 for word, ind in w2i.iteritems()}
        self.pos = {word: ind + 3 for ind, word in enumerate(pos)}
        self.pos['.'] = self.pos['NN']
        self.headFlag = options.headFlag
        self.rlMostFlag = options.rlMostFlag
        self.rlFlag = options.rlFlag
        self.k = options.window
        self.hidden_units = options.hidden_units
        self.hidden2_units = options.hidden2_units
        self.vocab['*PAD*'] = 1
        self.pos['*PAD*'] = 1
        self.external_embedding = None
        self.name_entity_dims = 1

        if options.external_embedding is not None:
            external_embedding_fp = open(options.external_embedding, 'r')
            external_embedding_fp.readline()
            self.external_embedding = {line.split(' ')[0]: [float(f) for f in line.strip().split(' ')[1:]] for line in
                                       external_embedding_fp}
            external_embedding_fp.close()

            self.edim = len(self.external_embedding.values()[0])
            self.noextrn = [0.0 for _ in xrange(self.edim)]
            self.extrnd = {word: i + 3 for i, word in enumerate(self.external_embedding)}
            self.elookup = self.name_entity_model.add_lookup_parameters(
                (len(self.external_embedding) + 3, self.edim))  # not coming here
            for word, i in self.extrnd.iteritems():
                self.elookup.init_row(i, self.external_embedding[word])
            self.extrnd['*PAD*'] = 1
            self.extrnd['*INITIAL*'] = 2

        dims = self.wdims + self.pdims + (self.edim if self.external_embedding is not None else 0)
        self.blstmFlag = options.blstmFlag
        self.bibiFlag = options.bibiFlag
        self.nnvecs = (1 if self.headFlag else 0) + (2 if self.rlFlag or self.rlMostFlag else 0)
        self.__init_name_entity_model(options)
        if self.bibiFlag:
            self.surfaceBuilders = [VanillaLSTMBuilder(1, dims, self.ldims * 0.5, self.name_entity_model),
                                    VanillaLSTMBuilder(1, dims, self.ldims * 0.5, self.name_entity_model)]
            self.bsurfaceBuilders = [VanillaLSTMBuilder(1, self.ldims, self.ldims * 0.5, self.name_entity_model),
                                     VanillaLSTMBuilder(1, self.ldims, self.ldims * 0.5, self.name_entity_model)]
        elif self.blstmFlag:
            if self.layers > 0:
                self.surfaceBuilders = [VanillaLSTMBuilder(self.layers, dims, self.ldims * 0.5, self.name_entity_model), LSTMBuilder(self.layers, dims, self.ldims * 0.5, self.name_entity_model)]
            else:
                self.surfaceBuilders = [SimpleRNNBuilder(1, dims, self.ldims * 0.5, self.name_entity_model), LSTMBuilder(1, dims, self.ldims * 0.5, self.name_entity_model)]

        self.vocab['*INITIAL*'] = 2
        self.pos['*INITIAL*'] = 2

        self.wlookup = self.name_entity_model.add_lookup_parameters((len(words) + 3, self.wdims))
        self.plookup = self.name_entity_model.add_lookup_parameters((len(pos) + 3, self.pdims)) # every part of speach is represented by 25 dimention
        self.rlookup = self.name_entity_model.add_lookup_parameters((len(rels), self.rdims))  # two type of relations - with/without root , each represented by 25

        self.word2lstm = self.name_entity_model.add_parameters((self.ldims, self.wdims + self.pdims + (self.edim if self.external_embedding is not None else 0)))
        self.word2lstmbias = self.name_entity_model.add_parameters((self.ldims))
        self.lstm2lstm = self.name_entity_model.add_parameters((self.ldims, self.ldims * self.nnvecs + self.rdims))
        self.lstm2lstmbias = self.name_entity_model.add_parameters((self.ldims))

    def __evaluate(self, word):
        input = concatenate(list(chain(*([word.lstms]))))
        # new ra evaluation
        middle_exp = self.activation(self.hidLayer.expr() * input + self.hidBias.expr())
        name_entity_output = (self.outLayer.expr() * middle_exp + self.outBias.expr())
        name_entity_output_value = name_entity_output.value()
        seg_ret = [
            [i, name_entity_output_value[i], name_entity_output[i]] for i in range(6)
            ]

        return  seg_ret

    def Save(self, file_name):
        self.name_entity_model.save(file_name)

    def Load(self, file_name):
        self.name_entity_model.populate(file_name)

    def Init(self):
        evec = self.elookup[1] if self.external_embedding is not None else None
        paddingWordVec = self.wlookup[1]
        paddingPosVec = self.plookup[1] if self.pdims > 0 else None

        paddingVec = tanh(self.word2lstm.expr() * concatenate(
            filter(None, [paddingWordVec, paddingPosVec, evec])) + self.word2lstmbias.expr())
        self.empty = paddingVec if self.nnvecs == 1 else concatenate([paddingVec for _ in xrange(self.nnvecs)])

    def getWordEmbeddings(self, sentence, train):
        for root in sentence:
            c = float(self.wordsCount.get(root.norm, 0))
            dropFlag = not train or (random.random() < (c / (0.25 + c)))
            root.wordvec = self.wlookup[int(self.vocab.get(root.norm, 0)) if dropFlag else 0]
            if root.pos == "TMOD":
                root.pos = "-"
            if root.pos == "PARATAXIS":
                root.pos = "-"
            if root.pos == "REF":
                root.pos = "-"
            try:
                root.posvec = self.plookup[int(self.pos[root.pos])] if self.pdims > 0 else None
            except Exception as e:
                print("missing pos {pos}".format(pos=root.pos))
                print(self.pos.keys())
                pass
            if self.external_embedding is not None:
                if root.form in self.external_embedding:
                    root.evec = self.elookup[self.extrnd[root.form]]
                elif root.norm in self.external_embedding:
                    root.evec = self.elookup[self.extrnd[root.norm]]
                else:
                    root.evec = self.elookup[0]
            else:
                root.evec = None
            root.ivec = concatenate(filter(None, [root.wordvec, root.posvec, root.evec]))

        if self.blstmFlag:
            forward = self.surfaceBuilders[0].initial_state()
            backward = self.surfaceBuilders[1].initial_state()

            for froot, rroot in zip(sentence, reversed(sentence)):
                forward = forward.add_input(froot.ivec)
                backward = backward.add_input(rroot.ivec)
                froot.fvec = forward.output()
                rroot.bvec = backward.output()
            for root in sentence:
                root.vec = concatenate([root.fvec, root.bvec])

            if self.bibiFlag:
                bforward = self.bsurfaceBuilders[0].initial_state()
                bbackward = self.bsurfaceBuilders[1].initial_state()

                for froot, rroot in zip(sentence, reversed(sentence)):
                    bforward = bforward.add_input(froot.vec)
                    bbackward = bbackward.add_input(rroot.vec)
                    froot.bfvec = bforward.output()
                    rroot.bbvec = bbackward.output()
                for root in sentence:
                    root.vec = concatenate([root.bfvec, root.bbvec])

        else:
            for root in sentence:
                root.ivec = (self.word2lstm.expr() * root.ivec) + self.word2lstmbias.expr()
                root.vec = tanh(root.ivec)

    def init_conll_sentence(self, sentence):
        conll_sentence = [entry for entry in sentence if isinstance(entry, utils.ConllEntry)]
        self.getWordEmbeddings(conll_sentence, False)
        for root in conll_sentence:
            root.lstms = [root.vec for _ in xrange(self.nnvecs)]
        return conll_sentence


    def Predict(self, conll_path):
        total = 0
        res = [[0,0,0] for i in range(6)]

        with open(conll_path, 'r') as conllFP:
            for iSentence, sentence in enumerate(read_conll(conllFP, False)):
                self.Init()

                conll_sentence = self.init_conll_sentence(sentence)

                for iword, word in enumerate(conll_sentence):
                    total += 1
                    scores = self.__evaluate(word)
                    best = max((s for s in scores), key=itemgetter(1))
                    res[best[0]][1] += 1 # parser
                    res[word.name_entity - 1][2] += 1 # gold
                    if best[0] + 1 == word.name_entity:
                        res[best[0]][0] += 1

                renew_cg()
                yield sentence
        f1_scores = []
        for i in range(6) :
            print "res for answer " + str(i+1)
            tp = res[i][0]
            if res[i][1] == 0:
                precision = 0
            else:
                precision = tp*1.0/res[i][1]
            if res[i][2] == 0:
                recall = 0
            else:
                recall = tp*1.0/res[i][2]
            if precision + recall == 0:
               score = 0
            else:
               score =  2.0 * precision * recall / (precision + recall)
            f1_scores.append(score)
            print "tp: {tp}, fp: {fp}, fn : {fn}. precision: {precision}, recall: {recall}==> f1: {score}".format(precision=precision, recall=recall, score=score, tp=tp, fp=str(res[i][1]-tp), fn=str(res[i][2]-tp))
        f1_total = sum(f1_scores)/ len(f1_scores)
        print "Full name_entity f1 score is {score}".format(score=str(f1_total))

    def Train(self, conll_path):
        ner_mloss = 0.0
        mloss = 0.0
        eerrors = 0
        lerrors = 0
        etotal = 0

        start = time.time()

        with open(conll_path, 'r') as conllFP:
            shuffledData = list(read_conll(conllFP, True))
            random.shuffle(shuffledData)
            ner_errs= []

            self.Init()
            counter =0
            for iSentence, sentence in enumerate(shuffledData):
                if iSentence % 100 == 0 and iSentence != 0:
                    print 'Processing sentence number:', iSentence, 'Loss:', ner_mloss / etotal, 'Errors:', (float(eerrors)) / etotal, 'Labeled Errors:', (float(lerrors) / etotal) , 'Time', time.time()-start
                    start = time.time()
                    eerrors = 0
                    etotal = 0
                    lerrors = 0

                conll_sentence = self.init_conll_sentence(sentence)
                losses = {}
                for iword, word in enumerate(conll_sentence):
                    scores = self.__evaluate(word)
                    losses[iword] = scalarInput(0)
                    best = max((s for s in scores), key=itemgetter(1))

                    if word.name_entity != best[0] + 1:
                        ner_loss = best[2] - scores[word.name_entity-1][2]
                        ner_errs.append(ner_loss)
                        losses[iword]= best[1] - scores[word.name_entity-1][1]
                        ner_mloss += 1.0 + best[1] - scores[word.name_entity-1][1]
                        eerrors += 1

                    etotal += 1
                if len(ner_errs) > 0 :
                    ner_eerrs = esum(ner_errs)
                    ner_eerrs.backward()
                    self.trainer.update()
                    ner_errs = []
                    renew_cg()

            print "counter of the times not segment had higher score : {c}".format(c=counter)
            self.trainer.update()
            print "ner Loss ", ner_mloss / iSentence
            print "Loss: ", mloss/iSentence
