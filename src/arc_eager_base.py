from dynet import *
from utils import ParseForest, read_conll, write_conll
from operator import itemgetter
from itertools import chain
import utils, time, random
import numpy as np
from named_entity_tagger import NameEntityTagger
from bilstm import BiLSTM


class ArcEagerLSTMBase(BiLSTM):
    def init_name_entity_model(self, words, pos, rels, w2i, options):
        self.name_entity_model = NameEntityTagger(words, pos, rels, w2i, options)
        if self.is_name_entity_use:
            if self.name_entity_join:
                pass
            else:
                self.name_entity_model.Load(r"src/name_entity_model.model30")

    def __init__(self, words, pos, rels, w2i, options):
        self.name_entity_join=options.name_entity_join
        self.named_entity_gold=options.named_entity_gold
        self.is_name_entity_use = options.is_name_entity_use
        self.name_entity_middle_layer=options.name_entity_middle_layer
        self.name_entity_middle_layer=options.name_entity_middle_layer
        self.name_entity_scores_layer=options.name_entity_scores_layer
        if self.name_entity_scores_layer and self.name_entity_middle_layer:
            print "illegal state"
            exit(0)
        if self.is_name_entity_use:
            if options.name_entity_middle_layer:
                self.name_entity_dims = options.hidden_units
            elif self.name_entity_scores_layer:
                self.name_entity_dims = 6
            else:
                self.name_entity_dims = 1
        else:
            self.name_entity_dims = 0
        self.is_dynamic = False
        self.number_of_transitions = 4

        self.number_of_non_relational_transitions = 2
        self.number_of_relational_transitions = self.number_of_transitions - self.number_of_non_relational_transitions

        # transitions:
        self.SHIFT=0
        self.LEFT_ARC=1
        self.RIGHT_ARC=2
        self.REDUCE = 3
        # --------------

        self.model = Model()
        self.trainer = AdamTrainer(self.model)
        random.seed(1)

        self.activations = {'tanh': tanh, 'sigmoid': logistic, 'relu': rectify, 'tanh3': (lambda x: tanh(cwise_multiply(cwise_multiply(x, x), x)))}
        self.activation = self.activations[options.activation]

        self.oracle = options.oracle
        self.ldims = options.lstm_dims * 2
        self.wdims = options.wembedding_dims
        self.pdims = options.pembedding_dims
        self.rdims = options.rembedding_dims
        self.layers = options.lstm_layers
        self.wordsCount = words
        self.vocab = {word: ind+3 for word, ind in w2i.iteritems()}
        self.pos = {word: ind+3 for ind, word in enumerate(pos)}
        self.pos['.'] = self.pos['NN']
        #self.pos['VMOD'] = self.pos['MOD']
        self.rels = {word: ind for ind, word in enumerate(rels)}
        self.irels = rels

        self.headFlag = options.headFlag
        self.rlMostFlag = options.rlMostFlag
        self.rlFlag = options.rlFlag
        self.k = options.window

        self.nnvecs = (1 if self.headFlag else 0) + (2 if self.rlFlag or self.rlMostFlag else 0)

        self.external_embedding = None
        if options.external_embedding is not None:
            external_embedding_fp = open(options.external_embedding,'r')
            external_embedding_fp.readline()
            self.external_embedding = {line.split(' ')[0] : [float(f) for f in line.strip().split(' ')[1:]] for line in external_embedding_fp}
            external_embedding_fp.close()

            self.edim = len(self.external_embedding.values()[0])
            self.noextrn = [0.0 for _ in xrange(self.edim)]
            self.extrnd = {word: i + 3 for i, word in enumerate(self.external_embedding)}
            self.elookup = self.model.add_lookup_parameters((len(self.external_embedding) + 3, self.edim))
            for word, i in self.extrnd.iteritems():
                self.elookup.init_row(i, self.external_embedding[word])
            self.extrnd['*PAD*'] = 1
            self.extrnd['*INITIAL*'] = 2

            print 'Load external embedding. Vector dimensions', self.edim

        dims = self.name_entity_dims + self.wdims + self.pdims + (self.edim if self.external_embedding is not None else 0)
        self.blstmFlag = options.blstmFlag
        self.bibiFlag = options.bibiFlag

        if self.bibiFlag:
            self.surfaceBuilders = [VanillaLSTMBuilder(1, dims, self.ldims * 0.5, self.model),
                                    VanillaLSTMBuilder(1, dims, self.ldims * 0.5, self.model)]
            self.bsurfaceBuilders = [VanillaLSTMBuilder(1, self.ldims, self.ldims * 0.5, self.model),
                                     VanillaLSTMBuilder(1, self.ldims, self.ldims * 0.5, self.model)]
        elif self.blstmFlag:
            if self.layers > 0:
                self.surfaceBuilders = [VanillaLSTMBuilder(self.layers, dims, self.ldims * 0.5, self.model), LSTMBuilder(self.layers, dims, self.ldims * 0.5, self.model)]
            else:
                self.surfaceBuilders = [SimpleRNNBuilder(1, dims, self.ldims * 0.5, self.model), LSTMBuilder(1, dims, self.ldims * 0.5, self.model)]

        self.hidden_units = options.hidden_units
        self.hidden2_units = options.hidden2_units
        self.vocab['*PAD*'] = 1
        self.pos['*PAD*'] = 1

        self.vocab['*INITIAL*'] = 2
        self.pos['*INITIAL*'] = 2
        self.pos['.'] = self.pos['NN']
        self.wlookup = self.model.add_lookup_parameters((len(words) + 3, self.wdims))
        self.plookup = self.model.add_lookup_parameters((len(pos) + 3, self.pdims))
        self.rlookup = self.model.add_lookup_parameters((len(rels), self.rdims))

        self.word2lstm = self.model.add_parameters((self.ldims, self.name_entity_dims + self.wdims + self.pdims + (self.edim if self.external_embedding is not None else 0)))
        self.word2lstmbias = self.model.add_parameters((self.ldims))
        self.lstm2lstm = self.model.add_parameters((self.ldims, self.ldims * self.nnvecs + self.rdims))
        self.lstm2lstmbias = self.model.add_parameters((self.ldims))
        self.nameEntitylookup = self.model.add_lookup_parameters((6 + 1, self.name_entity_dims))
        self.hidLayer = self.model.add_parameters((self.hidden_units, self.ldims * self.nnvecs * (self.k + 1)))
        self.hidBias = self.model.add_parameters((self.hidden_units))

        self.hid2Layer = self.model.add_parameters((self.hidden2_units, self.hidden_units))
        self.hid2Bias = self.model.add_parameters((self.hidden2_units))

        self.outLayer = self.model.add_parameters((self.number_of_transitions, self.hidden2_units if self.hidden2_units > 0 else self.hidden_units))
        self.outBias = self.model.add_parameters((self.number_of_transitions))

        self.rhidLayer = self.model.add_parameters((self.hidden_units, self.ldims * self.nnvecs * (self.k + 1)))
        self.rhidBias = self.model.add_parameters((self.hidden_units))

        self.rhid2Layer = self.model.add_parameters((self.hidden2_units, self.hidden_units))
        self.rhid2Bias = self.model.add_parameters((self.hidden2_units))

        self.routLayer = self.model.add_parameters(((self.number_of_relational_transitions) * (len(self.irels) + 0) + self.number_of_non_relational_transitions, self.hidden2_units if self.hidden2_units > 0 else self.hidden_units))
        self.routBias = self.model.add_parameters(((self.number_of_relational_transitions) * (len(self.irels) + 0) + self.number_of_non_relational_transitions))
        self.init_name_entity_model(words, pos, rels, w2i, options)

    def get_reduce_index(self):
        # left arc and right arc are before reduce
        return len(self.irels) * 2 + 1

    def get_reduce_all_index(self):
        # left arc and right arc are before reduce
        return len(self.irels) * 2 + 2

    def is_reduce_all(self, stack):
        for item in stack.roots[1:]:
            if item.parent is None:
                return False
        return True

    def __evaluate(self, stack, buf, train):
        topStack = [ stack.roots[-i-1].lstms if len(stack) > i else [self.empty] for i in xrange(self.k) ]
        topBuffer = [ buf.roots[i].lstms if len(buf) > i else [self.empty] for i in xrange(1) ]

        # The input to the neural network is based on the top of the stack and the buffer.
        input = concatenate(list(chain(*(topStack + topBuffer))))

        if self.hidden2_units > 0:
            routput = (self.routLayer.expr() * self.activation(self.rhid2Bias.expr() + self.rhid2Layer.expr() * self.activation(self.rhidLayer.expr() * input + self.rhidBias.expr())) + self.routBias.expr())
        else:
            routput = (self.routLayer.expr() * self.activation(self.rhidLayer.expr() * input + self.rhidBias.expr()) + self.routBias.expr())

        if self.hidden2_units > 0:
            output = (self.outLayer.expr() * self.activation(self.hid2Bias.expr() + self.hid2Layer.expr() * self.activation(self.hidLayer.expr() * input + self.hidBias.expr())) + self.outBias.expr())
        else:
            output = (self.outLayer.expr() * self.activation(self.hidLayer.expr() * input + self.hidBias.expr()) + self.outBias.expr())

        scrs, uscrs = routput.value(), output.value()

        #transition conditions
        left_arc_conditions = len(stack) > 0 and len(buf) > 0 and stack.roots[-1].id != 0 and stack.roots[-1].parent is None
        right_arc_conditions = len(stack) > 0 and len(buf) > 0 and ((len([item for item in stack.roots[1:] if item.parent is None]) == 0) or (len(buf) > 1))
        shift_conditions = len(buf) >1
        reduce_conditions = len(stack) > 0 and stack.roots[-1].parent is not None and stack.roots[-1].id != -1

        if len(stack) > 0 and stack.roots[-1].parent:
            pass # sanity check

        uscrs_shift = uscrs[self.SHIFT]
        uscrs_left_arc = uscrs[self.LEFT_ARC]
        uscrs_right_arc = uscrs[self.RIGHT_ARC]
        uscrs_reduce = uscrs[self.REDUCE]

        if train:
            output_shift = output[self.SHIFT]
            output_left_arc = output[self.LEFT_ARC]
            output_right_arc = output[self.RIGHT_ARC]
            output_reduce = output[self.REDUCE]

            # The return value is a list of 3 lists:
            # 1) contains all the (relation, score + uscore, routput + output1) for left arc
            # 2) contains all the (relation, score + uscore, routput + output2) for right arc
            # 3) contains (None, score + uscore, routput + output0)

            try:
                ret = [[(rel, self.LEFT_ARC, scrs[self.LEFT_ARC + j * self.number_of_relational_transitions] + uscrs_left_arc,
                         routput[self.LEFT_ARC + j * self.number_of_relational_transitions] + output_left_arc) for j, rel in
                        enumerate(self.irels)] if left_arc_conditions else [],
                       [(rel, self.RIGHT_ARC, scrs[self.RIGHT_ARC + j * self.number_of_relational_transitions] + uscrs_right_arc,
                         routput[self.RIGHT_ARC + j * self.number_of_relational_transitions] + output_right_arc) for j, rel in
                        enumerate(self.irels)] if right_arc_conditions else [],
                       [(None, self.SHIFT, scrs[self.SHIFT] + uscrs_shift,
                         routput[self.SHIFT] + output_shift)] if shift_conditions else [],
                       [(None, self.REDUCE, scrs[self.get_reduce_index()] + uscrs_reduce,
                         routput[self.get_reduce_index()] + output_reduce)] if reduce_conditions else []
                       ]
            except Exception as e:
                pass
        else:
            s1,r1 = max(zip(scrs[self.LEFT_ARC::self.number_of_relational_transitions],self.irels))
            s2,r2 = max(zip(scrs[self.RIGHT_ARC::self.number_of_relational_transitions],self.irels))
            s1 += uscrs_left_arc
            s2 += uscrs_right_arc
            ret = [[(r1, self.LEFT_ARC, s1)] if left_arc_conditions else [],
                   [(r2, self.RIGHT_ARC, s2)] if right_arc_conditions else [],
                   [(None, self.SHIFT, scrs[self.SHIFT] + uscrs_shift)] if shift_conditions else [],
                   [(None, self.REDUCE, scrs[self.get_reduce_index()] + uscrs_reduce)] if reduce_conditions else [],

                   ]
        return ret


    def Save(self, filename):
        self.model.save(filename)


    def Load(self, filename):
        self.model.populate(filename) #self.model.load(filename)


    def Init(self):
        evec = self.elookup[1] if self.external_embedding is not None else None
        paddingWordVec = self.wlookup[1]
        paddingPosVec = self.plookup[1] if self.pdims > 0 else None
        paddingNameEntityVec = None
        self.name_entity_model.Init()
        if self.is_name_entity_use:
            if self.name_entity_middle_layer:
                name_entity_input = self.name_entity_model.empty
                paddingNameEntityVec = self.activation(
                    self.name_entity_model.hidLayer.expr() * name_entity_input + self.name_entity_model.hidBias.expr())
            else:
                paddingNameEntityVec = self.nameEntitylookup[0]

        paddingVec = tanh(self.word2lstm.expr() * concatenate(
            filter(None, [paddingWordVec, paddingPosVec, evec, paddingNameEntityVec])) + self.word2lstmbias.expr())
        self.empty = paddingVec if self.nnvecs == 1 else concatenate([paddingVec for _ in xrange(self.nnvecs)])


    def getWordEmbeddings(self, sentence, train):
        named_entity_loss_val = 0
        name_entity_loss = []
        self.name_entity_model.getWordEmbeddings(sentence, train)
        for root in sentence:
            c = float(self.wordsCount.get(root.norm, 0))
            dropFlag = not train or (random.random() < (c / (0.25 + c)))
            root.wordvec = self.wlookup[int(self.vocab.get(root.norm, 0)) if dropFlag else 0]
            if root.pos == "TMOD":
                root.pos= "-"
            if root.pos == "PARATAXIS":
                root.pos =  "-"
            if root.pos == "REF":
                root.pos = "-"
            try:
                root.posvec = self.plookup[int(self.pos[root.pos])] if self.pdims > 0 else None
            except Exception as e:
                print("missing pos {pos}".format(pos=root.pos))
                print(self.pos.keys())
                pass
            if self.external_embedding is not None:
                # if not dropFlag and random.random() < 0.5:
                #    root.evec = self.elookup[0]
                if root.form in self.external_embedding:
                    root.evec = self.elookup[self.extrnd[root.form]]
                elif root.norm in self.external_embedding:
                    root.evec = self.elookup[self.extrnd[root.norm]]
                else:
                    root.evec = self.elookup[0]
            else:
                root.evec = None
            root.name_entity_vec = None

            if self.is_name_entity_use:

                name_entity_input = concatenate(list(chain(*([root.vec for _ in xrange(self.nnvecs)]))))
                name_entity_middle_exp = self.activation(
                    self.name_entity_model.hidLayer.expr() * name_entity_input + self.name_entity_model.hidBias.expr())
                output = (self.name_entity_model.outLayer.expr() * name_entity_middle_exp + self.name_entity_model.outBias.expr())
                name_entity_output_value = output.value()
                scores = [
                        [i, name_entity_output_value[i], output[i]] for i in range(6)
                        ]
                best = max((s for s in scores), key=itemgetter(1))
                if root.name_entity != best[0] + 1:
                    if train:
                        name_entity_loss.append(best[2] - scores[root.name_entity - 1][2])
                        named_entity_loss_val += best[1] - scores[root.name_entity - 1][1]
                if not train:
                    name_entity_loss.append([root.name_entity-1, best[0]])

                if self.name_entity_middle_layer:
                    root.name_entity_vec = name_entity_middle_exp
                elif self.name_entity_scores_layer:
                    root.name_entity_vec = output
                else:
                    if self.named_entity_gold:
                        root.name_entity_vec=self.nameEntitylookup[root.name_entity]
                    else:
                        root.name_entity_vec=self.nameEntitylookup[name_entity_output_value.index(max(name_entity_output_value)) + 1]
            root.ivec = concatenate(filter(None, [root.wordvec, root.posvec, root.evec, root.name_entity_vec]))

        if self.blstmFlag:
            forward = self.surfaceBuilders[0].initial_state()
            backward = self.surfaceBuilders[1].initial_state()

            for froot, rroot in zip(sentence, reversed(sentence)):
                forward = forward.add_input(froot.ivec)
                backward = backward.add_input(rroot.ivec)
                froot.fvec = forward.output()
                rroot.bvec = backward.output()
            for root in sentence:
                root.vec = concatenate([root.fvec, root.bvec])

            if self.bibiFlag:
                bforward = self.bsurfaceBuilders[0].initial_state()
                bbackward = self.bsurfaceBuilders[1].initial_state()

                for froot, rroot in zip(sentence, reversed(sentence)):
                    bforward = bforward.add_input(froot.vec)
                    bbackward = bbackward.add_input(rroot.vec)
                    froot.bfvec = bforward.output()
                    rroot.bbvec = bbackward.output()
                for root in sentence:
                    root.vec = concatenate([root.bfvec, root.bbvec])

        else:
            for root in sentence:
                root.ivec = (self.word2lstm.expr() * root.ivec) + self.word2lstmbias.expr()
                root.vec = tanh(root.ivec)
        return name_entity_loss, named_entity_loss_val

    def init_stack_buffer(self, sentence, train=False):
        conll_sentence = [entry for entry in sentence if isinstance(entry, utils.ConllEntry)]
        name_entity_loss, named_entity_loss_val = self.getWordEmbeddings(conll_sentence, train)
        stack = ParseForest([conll_sentence[0]])
        buf = ParseForest(conll_sentence[1:])
        for root in conll_sentence:
            root.lstms = [root.vec for _ in xrange(self.nnvecs)]
        return stack, buf, name_entity_loss, named_entity_loss_val


    def evaluate_ra(self, scores, stack, buf, best):
        if len(scores[4]) > 0:
            # can do reduce all
            # Now need to check if segmentation point:
                s0 = stack.roots[-1]
                if len([item for item in buf.roots if item.parent_id != 0 and item.parent_id < s0.id ]) == 0 and len([item for item in stack.roots if item.id != 0 and item.parent_id > s0.id]) == 0:
                        # reduce all is the correct action
                    if best[1] == self.REDUCE_ALL:
                        print "Made correctly reduce all with score: "+ str(scores)
                    elif best[1] == self.REDUCE :
                        print "Made reduce instead of reduce all "+ str(scores)
                    else:
                        print "Made else " + str(best[1]) + " instead of reduce all " + str(scores)



    def Predict(self, conll_path):
        confuse_matrix = [
            [0,0,0,0],
            [0,0,0,0],
            [0,0,0,0],
            [0,0,0,0]
        ]
        full_name_entity_losses = []
        with open(conll_path, 'r') as conllFP:
            for iSentence, sentence in enumerate(read_conll(conllFP, False)):
                self.Init()

                stack, buf, name_entity_loss, named_entity_loss_val = self.init_stack_buffer(sentence)
                full_name_entity_losses.extend(name_entity_loss)
                hoffset = 1 if self.headFlag else 0

                while not (len(buf) == 0 and len(stack) == 1):
                    scores = self.__evaluate(stack, buf, False)
                    try:
                        best = max(chain(*scores), key = itemgetter(2) )
                    except Exception as e:
                        pass
                    stack, buf, child, parent = self.move(best, stack, buf, hoffset)
                    confuse_matrix = self._evaluate_confuse(stack, buf, scores, confuse_matrix, best)

                renew_cg()
                yield sentence
        print "confuse_matrix"
        print confuse_matrix
        self.calculate_name_entity_loss(full_name_entity_losses)

    def calculate_name_entity_loss(self, choices):
        res = [[0, 0, 0] for i in range(6)]
        for item in choices:
            res[item[1]][1] += 1  # parser
            res[item[0]][2] += 1  # gold
            if item[0] == item[1]:
                res[item[0]][0] += 1

        f1_scores = []
        for i in range(6):
            print "res for answer " + str(i + 1)
            tp = res[i][0]
            if res[i][1] == 0:
                precision = 0
            else:
                precision = tp * 1.0 / res[i][1]
            if res[i][2] == 0:
                recall = 0
            else:
                recall = tp * 1.0 / res[i][2]
            if precision + recall == 0:
                score = 0
            else:
                score = 2.0 * precision * recall / (precision + recall)
            f1_scores.append(score)
            print "tp : {tp}, fp: {fp}, fn: {fn}, precision: {precision}, recall: {recall}==> f1: {score}".format(precision=precision, recall=recall,
                                                                                   score=score, tp=tp, fn=res[i][2]-tp, fp=res[i][1]-tp)
        f1_total = sum(f1_scores) / len(f1_scores)
        print "Full name_entity f1 score is {score}".format(score=str(f1_total))

    def _evaluate_confuse(self, stack, buf,  scores, confuse, best):
        try:
            alpha = stack.roots[:-2] if len(stack) > 2 else [] # the stack without the first two elements
            s1 = [stack.roots[-2]] if len(stack) > 1 else [] # the second element from the top
            s0 = [stack.roots[-1]] if len(stack) > 0 else [] # top element in the stack
            b = [buf.roots[0]] if len(buf) > 0 else [] # first element in the buffer
            beta = buf.roots[1:] if len(buf) > 1 else [] # the buffer without the first element
            # left cost: if - all the elements in the buffer and in the s1 that s0 is there child
            left_cost  = ( len([h for h in beta if h.id == s0[0].parent_id]) +
                           len([d for d in b + beta if d.parent_id == s0[0].id]) )  if len(scores[0]) > 0 else 1
            # right cost:
            right_cost = ( len([h for h in beta if len(buf) > 0 and h.id == b[0].parent_id]) +
                           len([d for d in s0+s1+alpha if len(buf) > 0 and d.parent_id == b[0].id]) +
                           len([e for e in s1+alpha if len(buf) > 0 and e.id == b[0].parent_id]))  if len(scores[1]) > 0 else 1

            shift_cost = ( len([h for h in s0+s1 + alpha if  len(buf) > 0 and h.id == b[0].parent_id]) +
                           len([d for d in s0 + s1 + alpha if len(buf) > 0 and d.parent_id == b[0].id]) )  if len(scores[2]) > 0 else 1
            reduce_cost = ( len([h for h in b+beta if h.parent_id == s0[0].id]) )  if len(scores[3]) > 0 else 1
        except Exception as e:
            import pdb
            pdb.set_trace()
        costs = (shift_cost, left_cost, right_cost, reduce_cost, 1)

        try:
            bestValid = max((s for s in chain(*scores) if costs[s[1]] == 0 and (
            s[1] == self.SHIFT or s[1] == self.REDUCE  or s[0] == stack.roots[
                -1].relation)), key=itemgetter(2))

        except Exception as e:
            bestValid = None

        if bestValid:
            confuse[bestValid[1]][best[1]] += 1
        return confuse

    def move(self, best, stack, buf, hoffset):
        child=None
        parent=None
        if best[1] == self.SHIFT:  # shift - not changed
            if len(buf) == 0:
                pass
            stack.roots.append(buf.roots[0])
            del buf.roots[0]

        elif best[1] == self.LEFT_ARC:  # did not changed from arc hybrid
            child = stack.roots.pop()
            parent = buf.roots[0]

            child.pred_parent_id = parent.id
            child.pred_relation = best[0]
            child.parent = parent
            bestOp = 0
            if self.rlMostFlag:
                parent.lstms[bestOp + hoffset] = child.lstms[bestOp + hoffset]
            if self.rlFlag:
                parent.lstms[bestOp + hoffset] = child.vec

        elif best[1] == self.RIGHT_ARC:
            if len(buf) == 0:
                pass
            child = buf.roots[0]
            parent = stack.roots[-1]
            child.parent = parent
            child.pred_parent_id = parent.id
            child.pred_relation = best[0]

            # The arc eager added shift methodology to the right-arc - inserting the first element in the
            # buffer to the stack
            stack.roots.append(buf.roots[0])
            del buf.roots[0]

            bestOp = 1
            if self.rlMostFlag:
                parent.lstms[bestOp + hoffset] = child.lstms[bestOp + hoffset]
            if self.rlFlag:
                parent.lstms[bestOp + hoffset] = child.vec
        elif best[1] == self.REDUCE:
            if stack.roots[-1].parent is None:
                print "reduce when no parent"
            stack.roots.pop()

        return stack, buf, child, parent

    def _calculate_reduce_all_score(self, stack, b, beta):
        sum_cost=0
        for item in stack.roots[1:]:
            sum_cost+= (len([h for h in b + beta if h.parent_id == item.id]))
        return sum_cost

    def Train(self, conll_path):
        mloss = 0.0
        errors = 0
        eloss = 0.0
        eerrors = 0
        lerrors = 0
        etotal = 0
        ninf = -float('inf')

        hoffset = 1 if self.headFlag else 0

        start = time.time()

        with open(conll_path, 'r') as conllFP:
            shuffledData = list(read_conll(conllFP, True))
            random.shuffle(shuffledData)

            errs = []
            eeloss = 0.0

            self.Init()
            named_entity_loss=0
            for iSentence, sentence in enumerate(shuffledData):
                if iSentence % 100 == 0 and iSentence != 0:
                    print 'Processing sentence number:', iSentence, 'Loss:', eloss / etotal, 'Errors:', (float(eerrors)) / etotal, 'Labeled Errors:', (float(lerrors) / etotal) , 'Time', time.time()-start
                    start = time.time()
                    eerrors = 0
                    eloss = 0.0
                    etotal = 0
                    lerrors = 0
                    ltotal = 0

                stack, buf, name_entity_loss, named_entity_loss_val = self.init_stack_buffer(sentence, train=True)
                named_entity_loss += 1.0 +named_entity_loss_val
                hoffset = 1 if self.headFlag else 0

                while not (len(buf) == 0 and len(stack) == 1):
                    scores = self.__evaluate(stack, buf, True)
                    scores.append([(None, self.number_of_transitions, ninf ,None)])

                    alpha = stack.roots[:-2] if len(stack) > 2 else [] # the stack without the first two elements
                    s1 = [stack.roots[-2]] if len(stack) > 1 else [] # the second element from the top
                    s0 = [stack.roots[-1]] if len(stack) > 0 else [] # top element in the stack
                    b = [buf.roots[0]] if len(buf) > 0 else [] # first element in the buffer
                    beta = buf.roots[1:] if len(buf) > 1 else [] # the buffer without the first element
                    # left cost: if - all the elements in the buffer and in the s1 that s0 is there child
                    left_cost  = ( len([h for h in beta if h.id == s0[0].parent_id]) +
                                   len([d for d in b + beta if d.parent_id == s0[0].id]) )  if len(scores[0]) > 0 else 1
                    # right cost:
                    right_cost = ( len([h for h in beta if h.id == b[0].parent_id]) +
                                   len([d for d in s0+s1+alpha if d.parent_id == b[0].id]) +
                                   len([e for e in s1+alpha if e.id == b[0].parent_id]))  if len(scores[1]) > 0 else 1

                    shift_cost = ( len([h for h in s0+s1 + alpha if h.id == b[0].parent_id]) +
                                   len([d for d in s0 + s1 + alpha if d.parent_id == b[0].id]) )  if len(scores[2]) > 0 else 1
                    reduce_cost = ( len([h for h in b+beta if h.parent_id == s0[0].id]) )  if len(scores[3]) > 0 else 1

                    costs = (shift_cost, left_cost, right_cost, reduce_cost, 1)


                    try:
                        bestValid = max((s for s in chain(*scores) if costs[s[1]] == 0 and (
                        s[1] == self.SHIFT or s[1] == self.REDUCE  or s[0] == stack.roots[
                            -1].relation)), key=itemgetter(2))

                    except Exception as e:
                        bestValid = None

                    bestWrong = max((s for s in chain(*scores) if costs[s[1]] != 0 or (
                        s[1] != self.SHIFT and s[1] != self.REDUCE  and s[0] != stack.roots[
                            -1].relation)), key=itemgetter(2))
                    if self.is_dynamic:
                        best = bestValid if bestValid is not None and ( (not self.oracle) or (bestValid[2] - bestWrong[2] > 1.0) or (bestValid[2] > bestWrong[2] and random.random() > 0.1) ) else bestWrong
                    else:
                        best = bestValid if bestValid is not None else bestWrong#and ( (not self.oracle) or (bestValid[2] - bestWrong[2] > 1.0) or (bestValid[2] > bestWrong[2] and random.random() > 0.1) ) else bestWrong

                    stack, buf, child, parent = self.move(best, stack, buf, hoffset)

                    if bestValid is not None and bestValid[2] < bestWrong[2] + 1.0:
                        loss = bestWrong[3] - bestValid[3]
                        mloss += 1.0 + bestWrong[2] - bestValid[2]
                        eloss += 1.0 + bestWrong[2] - bestValid[2]
                        errs.append(loss)

                    if best[1] != self.SHIFT and best[1] != self.REDUCE  and (
                            child.pred_parent_id != child.parent_id or child.pred_relation != child.relation):
                        lerrors += 1
                        if child.pred_parent_id != child.parent_id:
                            errors += 1
                            eerrors += 1

                    etotal += 1
                if self.name_entity_join:
                    errs.extend(name_entity_loss)
                if len(errs) > 50: # or True:
                    #eerrs = ((esum(errs)) * (1.0/(float(len(errs)))))
                    eerrs = esum(errs)
                    scalar_loss = eerrs.scalar_value()
                    eerrs.backward()
                    self.trainer.update()
                    errs = []
                    lerrs = []

                    renew_cg()
                    self.Init()

        if len(errs) > 0:
            eerrs = (esum(errs)) # * (1.0/(float(len(errs))))
            eerrs.scalar_value()
            eerrs.backward()
            self.trainer.update()

            errs = []
            lerrs = []

            renew_cg()

        self.trainer.update()
        print "Loss: ", mloss/iSentence
        print "ne loss: " + str(named_entity_loss)