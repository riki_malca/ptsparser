from optparse import OptionParser
from pts import PTSLSTM
from arc_eager_base import ArcEagerLSTMBase
from segmentation_classifier import SegmentationClassifier
from named_entity_tagger import NameEntityTagger
import pickle, utils, os, time, sys
from parsing_models.segmentation_model import SegmentationModel
from parsing_models.pts_model import PTSModel
from parsing_models.named_entity_model import NamedEntityModel

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("--train", dest="conll_train", help="Annotated CONLL train file", metavar="FILE",
                      default="../data/PTB_SD_3_3_0/train.conll")
    parser.add_option("--dev", dest="conll_dev", help="Annotated CONLL dev file", metavar="FILE",
                      default="../data/PTB_SD_3_3_0/dev.conll")
    parser.add_option("--test", dest="conll_test", help="Annotated CONLL test file", metavar="FILE",
                      default="../data/PTB_SD_3_3_0/test.conll")
    parser.add_option("--params", dest="params", help="Parameters file", metavar="FILE", default="params.pickle")
    parser.add_option("--extrn", dest="external_embedding", help="External embeddings", metavar="FILE")
    parser.add_option("--model", dest="model", help="Load/Save model file", metavar="FILE", default="barchybrid.model")
    parser.add_option("--flag_model", dest="flag_model", help="Load/Save flag_model file", metavar="FILE",
                      default="None")
    parser.add_option("--segmodel", dest="segmodel", help="Load/Save seg model file", metavar="FILE", default="None")
    parser.add_option("--segmodel_params", dest="segmodel_params", help="seg model params file", metavar="FILE",
                      default="None")
    parser.add_option("--ne_model", dest="ne_model", help="Load/Save ne model model file", metavar="FILE",
                      default=r"name_entity_model30")
    parser.add_option("--name_entity_model", dest="name_entity_model", help="Load/Save ra model file", metavar="FILE",
                      default="name_entity_barchybrid.model")
    parser.add_option("--wembedding", type="int", dest="wembedding_dims", default=100)
    parser.add_option("--pembedding", type="int", dest="pembedding_dims", default=25)
    parser.add_option("--rembedding", type="int", dest="rembedding_dims", default=25)
    parser.add_option("--epochs", type="int", dest="epochs", default=30)
    parser.add_option("--hidden", type="int", dest="hidden_units", default=100)
    parser.add_option("--hidden2", type="int", dest="hidden2_units", default=0)
    parser.add_option("--k", type="int", dest="window", default=3)
    parser.add_option("--lr", type="float", dest="learning_rate", default=0.1)
    parser.add_option("--outdir", type="string", dest="output", default="results")
    parser.add_option("--activation", type="string", dest="activation", default="tanh")
    parser.add_option("--lstmlayers", type="int", dest="lstm_layers", default=2)
    parser.add_option("--lstmdims", type="int", dest="lstm_dims", default=200)
    parser.add_option("--dynet-seed", type="int", dest="seed", default=7)
    parser.add_option("--disableoracle", action="store_false", dest="oracle", default=True)
    parser.add_option("--disableblstm", action="store_false", dest="blstmFlag", default=True)
    parser.add_option("--bibi-lstm", action="store_true", dest="bibiFlag", default=False)
    parser.add_option("--usehead", action="store_true", dest="headFlag", default=False)
    parser.add_option("--userlmost", action="store_true", dest="rlFlag", default=False)
    parser.add_option("--userl", action="store_true", dest="rlMostFlag", default=False)
    parser.add_option("--predict", action="store_true", dest="predictFlag", default=False)
    parser.add_option("--train_segmentation", action="store_true", dest="segmentationFlag", default=False)
    parser.add_option("--train_name_entity", action="store_true", dest="train_name_entity", default=False)
    parser.add_option("--dynet-mem", type="int", dest="cnn_mem", default=512)

    parser.add_option("--base_parser", action="store_true", dest="base_parser", default=False)
    parser.add_option("--is_segmentation_model_used", action="store_true", dest="is_segmentation_model_used",
                      default=False)
    parser.add_option("--is_extanded_segmentation_model_used", action="store_true",
                      dest="is_extanded_segmentation_model_used", default=False)
    parser.add_option("--is_dynamic", action="store_false", dest="is_dynamic", default=True)
    parser.add_option("--is_flag_model_used", type="string", dest="is_flag_model_used", default="None")
    parser.add_option("--mult_hidden_layer", type="int", dest="mult_hidden_layer", default=1)
    parser.add_option("--buf_wind", type="int", dest="buf_wind", default=1)
    parser.add_option("--two_hidden_layers", action="store_true", dest="two_hidden_layers", default=False)
    parser.add_option("--is_name_entity_use", action="store_true", dest="is_name_entity_use", default=False)
    parser.add_option("--named_entity_gold", action="store_true", dest="named_entity_gold", default=False)
    parser.add_option("--name_entity_middle_layer", action="store_true", dest="name_entity_middle_layer", default=False)
    parser.add_option("--name_entity_scores_layer", action="store_true", dest="name_entity_scores_layer", default=False)
    parser.add_option("--name_entity_join", action="store_true", dest="name_entity_join", default=False)

    (options, args) = parser.parse_args()
    print 'Using external embedding:', options.external_embedding

    segmodel_path = options.segmodel
    segmodel_params = options.segmodel_params

    segmentation_model = SegmentationModel(options)
    pts_model = PTSModel(options, segmentation_model)
    ner_model = NamedEntityModel(options)

    if options.segmentationFlag:
        if options.predictFlag:
            segmentation_model.test()
        else:
            segmentation_model.train()

    elif options.train_name_entity:
        if options.predictFlag:
            ner_model.test()
        else:
            ner_model.train()

    elif not options.predictFlag:
        if not (options.rlFlag or options.rlMostFlag or options.headFlag):
            print 'You must use either --userlmost or --userl or --usehead (you can use multiple)'
            sys.exit()
        pts_model.train()
    else:
        pts_model.test()

